package com.amdocs.webapp;
import static org.junit.Assert.*;
import org.junit.Test;

public class BasicCalculatorTest {
    @Test
    public void testAdd() throws Exception {

        int k= new BasicCalculator().add(3,6);
        assertEquals("Add", 9, k);

    }
    @Test
    public void testSub() throws Exception {

        int k= new BasicCalculator().sub(6,3);
        assertEquals("Sub", 3, k);

    }
}
